import React from 'react'

export default function AllEventPage() {
  return (
    <div className='container'>
        <section className="text-center container" style={{marginTop: "10%"}}>
            <h2 className="mb-5"><strong>Latest Projects</strong></h2>

            <div className="row">
            <div className="col-lg-4 col-md-12 mb-4">
                <div className="card">
                <div className="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                    <img src="https://mdbootstrap.com/img/new/standard/nature/184.jpg" alt='' className="img-fluid" />
                    <a href="#!">
                    <div className="mask" style={{backgroundColor: "rgba(251, 251, 251, 0.15)"}}></div>
                    </a>
                </div>
                <div className="card-body">
                    <h5 className="card-title">Post title</h5>
                    <p className="card-text">
                    Some quick example text to build on the card title and make up the bulk of the
                    card's content.
                    </p>
                    <a href="/projectdescrip" className="btn btn-primary">Read More</a>
                </div>
                </div>
            </div>

            <div className="col-lg-4 col-md-6 mb-4">
                <div className="card">
                <div className="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                    <img src="https://mdbootstrap.com/img/new/standard/nature/023.jpg" alt='' className="img-fluid" />
                    <a href="#!">
                    <div className="mask" style={{backgroundColor: "rgba(251, 251, 251, 0.15)"}}></div>
                    </a>
                </div>
                <div className="card-body">
                    <h5 className="card-title">Post title</h5>
                    <p className="card-text">
                    Some quick example text to build on the card title and make up the bulk of the
                    card's content.
                    </p>
                    <a href="/projectdescrip" className="btn btn-primary">Read More</a>
                </div>
                </div>
            </div>

            <div className="col-lg-4 col-md-6 mb-4">
                <div className="card">
                <div className="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                    <img src="https://mdbootstrap.com/img/new/standard/nature/111.jpg" alt='' className="img-fluid" />
                    <a href="#!">
                    <div className="mask" style={{backgroundColor: "rgba(251, 251, 251, 0.15)"}}></div>
                    </a>
                </div>
                <div className="card-body">
                    <h5 className="card-title">Post title</h5>
                    <p className="card-text">
                    Some quick example text to build on the card title and make up the bulk of the
                    card's content.
                    </p>
                    <a href="/projectdescrip" className="btn btn-primary">Read More</a>
                </div>
                </div>
            </div>
            </div>

            <div className="row">
            <div className="col-lg-4 col-md-12 mb-4">
                <div className="card">
                <div className="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                    <img src="https://mdbootstrap.com/img/new/standard/nature/184.jpg" alt='' className="img-fluid" />
                    <a href="#!">
                    <div className="mask" style={{backgroundColor: "rgba(251, 251, 251, 0.15)"}}></div>
                    </a>
                </div>
                <div className="card-body">
                    <h5 className="card-title">Post title</h5>
                    <p className="card-text">
                    Some quick example text to build on the card title and make up the bulk of the
                    card's content.
                    </p>
                    <a href="/projectdescrip" className="btn btn-primary">Read More</a>
                </div>
                </div>
            </div>

            <div className="col-lg-4 col-md-6 mb-4">
                <div className="card">
                <div className="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                    <img src="https://mdbootstrap.com/img/new/standard/nature/023.jpg" alt='' className="img-fluid" />
                    <a href="#!">
                    <div className="mask" style={{backgroundColor: "rgba(251, 251, 251, 0.15)"}}></div>
                    </a>
                </div>
                <div className="card-body">
                    <h5 className="card-title">Post title</h5>
                    <p className="card-text">
                    Some quick example text to build on the card title and make up the bulk of the
                    card's content.
                    </p>
                    <a href="/projectdescrip" className="btn btn-primary">Read More</a>
                </div>
                </div>
            </div>

            <div className="col-lg-4 col-md-6 mb-4">
                <div className="card">
                <div className="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                    <img src="https://mdbootstrap.com/img/new/standard/nature/111.jpg" alt='' className="img-fluid" />
                    <a href="#!">
                    <div className="mask" style={{backgroundColor: "rgba(251, 251, 251, 0.15)"}}></div>
                    </a>
                </div>
                <div className="card-body">
                    <h5 className="card-title">Post title</h5>
                    <p className="card-text">
                    Some quick example text to build on the card title and make up the bulk of the
                    card's content.
                    </p>
                    <a href="/projectdescrip" className="btn btn-primary">Read More</a>
                </div>
                </div>
            </div>
            </div>

            <div className="col-lg-4 col-md-6 mb-4">
            <div className="card">
                <div className="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                <img src="https://mdbootstrap.com/img/new/standard/nature/111.jpg" alt='' className="img-fluid" />
                <a href="#!">
                    <div className="mask" style={{backgroundColor: "rgba(251, 251, 251, 0.15)"}}></div>
                </a>
                </div>
                <div className="card-body">
                <h5 className="card-title">Post title</h5>
                <p className="card-text">
                    Some quick example text to build on the card title and make up the bulk of the
                    card's content.
                </p>
                <a href="/projectdescrip" className="btn btn-primary">Read More</a>
                </div>
            </div>
            </div>
           
        </section>
    </div>
  )
}
