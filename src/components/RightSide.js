import React from "react";
import "./styles.css";

function RightSide() {
  return (
    <div>
      <div id="right-bar">
        <h3 id="right-head">How to become a Volunteer?</h3>
     <div id="card-down">
   
 
  <div className="row g-0 bg-light position-relative " id="card-pos1">
  <div className="col-md-6 mb-md-0 p-md-4">
    <img
      src="https://mdbcdn.b-cdn.net/img/new/standard/city/041.webp"
      className="w-50"
      alt="hollywood sign"
    />

  </div>
  <div class="col-md-6 p-4 " id="right-pos1">
    <h5 class="mt-0">Guidelines</h5>
    <p>
      Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante
      sollicitudin. 
      faucibus.
    </p>
    <a href="/app10" class="stretched-link">Guidlines</a>
  </div>
</div>
<div className="row g-0 bg-light position-relative " id="card-pos2">
  <div className="col-md-6 mb-md-0 p-md-4">
    <img
      src="https://mdbcdn.b-cdn.net/img/new/standard/city/041.webp"
      className="w-50"
      alt="hollywood sign"
    />

  </div>
  <div class="col-md-6 p-4 " id="right-pos1">
    <h5 class="mt-0">Apply Now</h5>
    <p>
      Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante
      sollicitudin. 
      faucibus.
    </p>
    <a href="/application" class="stretched-link">Apply Now</a>
  </div>
</div>
<div className="row g-0 bg-light position-relative " id="card-pos2">
  <div className="col-md-6 mb-md-0 p-md-4">
    <img
      src="https://mdbcdn.b-cdn.net/img/new/standard/city/041.webp"
      className="w-50"
      alt="hollywood sign"
    />

  </div>
  <div class="col-md-6 p-4 " id="right-pos1">
    <h5 class="mt-0">Sign In</h5>
    <p>
      Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante
      sollicitudin. 
      faucibus.
    </p>
    <a href="/signin" class="stretched-link">Sign In</a>
  </div>
</div>
  </div>
  
  </div>
  </div>

  

   
  );
}

export default RightSide;
