import React from 'react'

export default function HomeAbout() {
  return (
    <div className='container'>
      <section>
        <div className="container-fluid gx-0" style={{height: "400px", marginTop: "55%"}}>
          <div style={{height: "400px", width: "50%", float: "left"}}>
            <img src="./img/1.jpg" alt='' width="100%" height="400px" className="image-responsive" />
          </div>
          <div className="container" style={{height: "400px", width: "50%", float: "right", backgroundColor: "rgb(218, 218, 218)"}}>
            <h1 style={{textAlign: "left", marginLeft: "5%", marginTop: "7%"}}>Who Are WE</h1>
            <p style={{textAlign: "justify", marginLeft: "5%", margiRight: "5%"}}>Lorem ipsum, dolor sit amet consectetur
              adipisicing elit. Rem nulla beatae consequatur, at natus es
              t, suscipit hic totam esse vero eaque a nihil culpa molestiae? Temporibus mollitia similique unde
              blanditiis?
              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rem nulla beatae consequatur, at natus es
              t, suscipit hic totam esse vero eaque a nihil culpa molestiae? Temporibus mollitia similique unde
              blanditiis?
              Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rem nulla beatae consequatur, at natus es
              t, suscipit hic totam esse vero eaque a nihil culpa molestiae? Temporibus mollitia similique unde
              blanditiis?</p>

            <button href="#" className="btn btn-primary d-block mr-0 ml-auto" style={{marginLeft: "5%"}}>Read more</button>
          </div>
        </div>
      </section>
    </div>
  )
}
