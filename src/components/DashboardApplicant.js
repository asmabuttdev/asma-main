import React from 'react'

function DashboardApplicant() {
  return (
    <div> <section className="row d-flex justify-content-center mb-4" style={{marginTop: "8%", width: "100%"}}>
    <div className="tab-content col-10 shadow-2-strong" style={{borderRadius: "10px"}}>
      <div className="row mx-2 my-5" >
          <div className="col-3 shadow-1-strong" style={{height: "780px", borderRadius: "30px"}}>
          
            <div className="mt-4 text-center">
              <h4>Dashboard</h4>
            </div>
          
          <div className="text-center mt-4 mb-3" >
              
              <img src="./img/school-india-children-ahmedabad.jpg" className="rounded-circle shadow-2-strong" width="150px" height="150px" alt="avatar" />    
              
              </div>
         
          <div className="text-center" style={{marginBottom: "25%"}}>
              
              <h6>Amjad Khan</h6>   
              
              </div>
          
            <div
              className="nav flex-column nav-pills text-center"
              id="v-pills-tab"
              role="tablist"
              aria-orientation="vertical" >
              <a
                className="nav-link active"
                id="v-pills-home-tab"
                data-mdb-toggle="pill"
                href="#v-pills-home"
                role="tab"
                aria-controls="v-pills-home"
                aria-selected="true"
                >Personal Info</a
              >
              <a
                className="nav-link"
                id="v-pills-profile-tab"
                data-mdb-toggle="pill"
                href="#v-pills-profile"
                role="tab"
                aria-controls="v-pills-profile"
                aria-selected="false"
                >My Applications</a>
              <a
                className="nav-link"
                id="v-pills-messages-tab"
                data-mdb-toggle="pill"
                href="#v-pills-messages"
                role="tab"
                aria-controls="v-pills-messages"
                aria-selected="false"
                >My Donations</a
              >
              <a
                className="nav-link"
                id="v-pills-setting-tab"
                data-mdb-toggle="pill"
                href="#v-pills-setting"
                role="tab"
                aria-controls="v-pills-setting"
                aria-selected="false"
                >Settings</a
              >
              
            </div>
            <div className="text-center mb-3" style={{marginTop: "50%"}}>
              <button className="btn btn-primary" style={{width: "120px"}}>
                  Logout
              </button>
          </div>
           
          </div>
        
          <div className="col-9">
  
  
       
            <div className="tab-content" id="v-pills-tabContent">
              <div
                className="tab-pane fade show active"
                id="v-pills-home"
                role="tabpanel"
                aria-labelledby="v-pills-home-tab"
              >
              <div className="mt-4 text-center">
                <h4>Personal Information</h4>
              </div>
                
              <form className="my-5 mx-4">
               
                
                  <div className="mb-4 " style={{width: "48.5%"}}>
                    <div className="form-outline">
                      <input type="text" id="form6Example1" className="form-control" />
                      <label className="form-label" htmlFor="form6Example1">UserName</label>
                    </div>
                  </div>
                
              
                
                <div className="row mb-4">
                  <div className="col">
                <div className="form-outline">
                  <input type="text" id="form6Example3" className="form-control" />
                  <label className="form-label" htmlFor="form6Example3">First Name</label>
                </div>
              </div>
              <div className="col">
                <div className="form-outline">
                  <input type="text" id="form6Example3" className="form-control" />
                  <label className="form-label" htmlFor="form6Example3">Last Name</label>
                </div>
              </div>
              </div>
              
               
                <div className="form-outline mb-4" style={{width: "48.5%"}}>
                  <input type="text" id="form6Example4" className="form-control" />
                  <label className="form-label" htmlFor="form6Example4">CNIC</label>
                </div>
             
                <div className="row mb-4">
                  <div className="col">
                <div className="form-outline">
                  <input type="email" id="form6Example5" className="form-control" />
                  <label className="form-label" htmlFor="form6Example5">Primary PhoneNo</label>
                </div>
              </div>
              <div className="col">
                <div className="form-outline">
                  <input type="email" id="form6Example5" className="form-control" />
                  <label className="form-label" htmlFor="form6Example5">Secondary PhoneNo</label>
                </div>
              </div>
              </div>
              
               
                
                <div className="form-outline mb-4">
                  <input type="number" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">Current Address</label>
                </div>
                <div className="form-outline mb-4">
                  <input type="number" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">Permanent Address</label>
                </div>
              
              
                
                <div className="row mb-4">
                  <div className="col">
                <div className="form-outline mb-4">
                  <input type="text" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">Province</label>
                </div>
              </div>
              <div className="col">
                <div className="form-outline mb-4">
                  <input type="text" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">City</label>
                </div>
              </div>
            </div>
              
              
                <div className="text-center mb-3" style={{marginTop: "24%"}}>
                <button type="submit" className="btn btn-primary mb-4 ">Save</button>
              </div>
              </form>
  
              </div>
  
            
              <div
                className="tab-pane fade"
                id="v-pills-profile"
                role="tabpanel"
                aria-labelledby="v-pills-profile-tab"
              >
                
              <div className="mt-4 text-center">
                <h4>Personal Information</h4>
              </div>
                
              <form className="my-5">
               
                
                  <div className="mb-4 " style={{width: "48.5%"}}>
                    <div className="form-outline">
                      <input type="text" id="form6Example1" className="form-control" />
                      <label className="form-label" htmlFor="form6Example1">UserName</label>
                    </div>
                  </div>
                
              
                
                <div className="row mb-4">
                  <div className="col">
                <div className="form-outline">
                  <input type="text" id="form6Example3" className="form-control" />
                  <label className="form-label" htmlFor="form6Example3">First Name</label>
                </div>
              </div>
              <div className="col">
                <div className="form-outline">
                  <input type="text" id="form6Example3" className="form-control" />
                  <label className="form-label" htmlFor="form6Example3">Last Name</label>
                </div>
              </div>
              </div>
              
                
                <div className="form-outline mb-4" style={{width: "48.5%"}}>
                  <input type="text" id="form6Example4" className="form-control" />
                  <label className="form-label" htmlFor="form6Example4">CNIC</label>
                </div>
              
                <div className="row mb-4">
                  <div className="col">
                <div className="form-outline">
                  <input type="email" id="form6Example5" className="form-control" />
                  <label className="form-label" htmlFor="form6Example5">Primary PhoneNo</label>
                </div>
              </div>
              <div className="col">
                <div className="form-outline">
                  <input type="email" id="form6Example5" className="form-control" />
                  <label className="form-label" htmlFor="form6Example5">Secondary PhoneNo</label>
                </div>
              </div>
              </div>
              
               
                
                <div className="form-outline mb-4">
                  <input type="number" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">Current Address</label>
                </div>
                <div className="form-outline mb-4">
                  <input type="number" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">Permanent Address</label>
                </div>
              
              
                <div className="row mb-4">
                  <div className="col">
                <div className="form-outline mb-4">
                  <input type="text" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">Province</label>
                </div>
              </div>
              <div className="col">
                <div className="form-outline mb-4">
                  <input type="text" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">City</label>
                </div>
              </div>
            </div>
              
               
                <div className="text-center mb-3" style={{marginTop: "24%"}}>
                <button type="submit" className="btn btn-primary mb-4 ">Save</button>
              </div>
              </form>
  
              </div>
  
              
              <div
                className="tab-pane fade"
                id="v-pills-messages"
                role="tabpanel"
                aria-labelledby="v-pills-messages-tab"
              >
                
              <div className="mt-4 text-center">
                <h4>Personal Information</h4>
              </div>
                
              <form className="my-5">
               
                
                  <div className="mb-4 " style={{width: "48.5%"}}>
                    <div className="form-outline">
                      <input type="text" id="form6Example1" className="form-control" />
                      <label className="form-label" htmlFor="form6Example1">UserName</label>
                    </div>
                  </div>
                
              
                <div className="row mb-4">
                  <div className="col">
                <div className="form-outline">
                  <input type="text" id="form6Example3" className="form-control" />
                  <label className="form-label" htmlFor="form6Example3">First Name</label>
                </div>
              </div>
              <div className="col">
                <div className="form-outline">
                  <input type="text" id="form6Example3" className="form-control" />
                  <label className="form-label" htmlFor="form6Example3">Last Name</label>
                </div>
              </div>
              </div>
              
                
                <div className="form-outline mb-4" style={{width: "48.5%"}}>
                  <input type="text" id="form6Example4" className="form-control" />
                  <label className="form-label" htmlFor="form6Example4">CNIC</label>
                </div>
              
            
                <div className="row mb-4">
                  <div className="col">
                <div className="form-outline">
                  <input type="email" id="form6Example5" className="form-control" />
                  <label className="form-label" htmlFor="form6Example5">Primary PhoneNo</label>
                </div>
              </div>
              <div className="col">
                <div className="form-outline">
                  <input type="email" id="form6Example5" className="form-control" />
                  <label className="form-label" htmlFor="form6Example5">Secondary PhoneNo</label>
                </div>
              </div>
              </div>
             
                
                <div className="form-outline mb-4">
                  <input type="number" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">Current Address</label>
                </div>
                <div className="form-outline mb-4">
                  <input type="number" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">Permanent Address</label>
                </div>
              
              
             
                <div className="row mb-4">
                  <div className="col">
                <div className="form-outline mb-4">
                  <input type="text" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">Province</label>
                </div>
              </div>
              <div className="col">
                <div className="form-outline mb-4">
                  <input type="text" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">City</label>
                </div>
              </div>
            </div>
              
             
                <div className="text-center mb-3" style={{marginTop: "24%"}}>
                <button type="submit" className="btn btn-primary mb-4 ">Save</button>
              </div>
              </form>
  
              </div>
  
             
              <div
                className="tab-pane fade"
                id="v-pills-setting"
                role="tabpanel"
                aria-labelledby="v-pills-setting-tab"
              >
                
              <div className="mt-4 text-center">
                <h4>Personal Information</h4>
              </div>
                
              <form className="my-5">
                
                
                  <div className="mb-4 " style={{width: "48.5%"}}>
                    <div className="form-outline">
                      <input type="text" id="form6Example1" className="form-control" />
                      <label className="form-label" htmlFor="form6Example1">UserName</label>
                    </div>
                  </div>
                
              
             
                <div className="row mb-4">
                  <div className="col">
                <div className="form-outline">
                  <input type="text" id="form6Example3" className="form-control" />
                  <label className="form-label" htmlFor="form6Example3">First Name</label>
                </div>
              </div>
              <div className="col">
                <div className="form-outline">
                  <input type="text" id="form6Example3" className="form-control" />
                  <label className="form-label" htmlFor="form6Example3">Last Name</label>
                </div>
              </div>
              </div>
              
                
                <div className="form-outline mb-4" style={{width: "48.5%"}}>
                  <input type="text" id="form6Example4" className="form-control" />
                  <label className="form-label" htmlFor="form6Example4">CNIC</label>
                </div>
              
               
                <div className="row mb-4">
                  <div className="col">
                <div className="form-outline">
                  <input type="email" id="form6Example5" className="form-control" />
                  <label className="form-label" htmlFor="form6Example5">Primary PhoneNo</label>
                </div>
              </div>
              <div className="col">
                <div className="form-outline">
                  <input type="email" id="form6Example5" className="form-control" />
                  <label className="form-label" htmlFor="form6Example5">Secondary PhoneNo</label>
                </div>
              </div>
              </div>
              
               
                
                <div className="form-outline mb-4">
                  <input type="number" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">Current Address</label>
                </div>
                <div className="form-outline mb-4">
                  <input type="number" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">Permanent Address</label>
                </div>
              
        
                <div className="row mb-4">
                  <div className="col">
                <div className="form-outline mb-4">
                  <input type="text" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">Province</label>
                </div>
              </div>
              <div className="col">
                <div className="form-outline mb-4">
                  <input type="text" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">City</label>
                </div>
              </div>
            </div>
              
                
                <div className="text-center mb-3" style={{marginTop: "24%"}}>
                <button type="submit" className="btn btn-primary mb-4 ">Save</button>
              </div>
              </form>
  
              </div>
              
            </div>
            
          </div>
        </div>
  </div>
  </section></div>
  )
}

export default DashboardApplicant