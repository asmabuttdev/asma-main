import React from 'react'

function Newvolunteerlist() {
  return (
    <div>
       <div className="bg-white" id="sidebar-wrapper">
              <div className="sidebar-heading text-center py-4 primary-text fs-4 fw-bold text-uppercase border-bottom"><i className />Social Welfare</div>
              <div className="list-group list-group-flush my-3">
                <a href="#" className="list-group-item list-group-item-action bg-transparent second-text active"><i className="fas fa-tachometer-alt me-2" />Dashboard</a>
                <a href="#" className="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i className />New Cases</a>
                <a href="#" className="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i className />Spam Cases</a>
                <a href="#" className="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i className />Approved Cases</a>
                <a href="#" className="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i className />New volunteers</a>
                <a href="#" className="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i className />Spam Volunteers</a>
                <a href="#" className="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i className />Approved Volunteer</a>
                <a href="#" className="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i className />Volunteer Reference</a>
                <a href="#" className="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i className />Projects</a>
                <a href="#" className="list-group-item list-group-item-action bg-transparent second-text fw-bold"><i className />Categories</a>
                <a href="#" className="list-group-item list-group-item-action bg-transparent text-danger fw-bold"><i className="fas fa-power-off me-2" />Logout</a>
              </div>
            </div>
            {/* /#sidebar-wrapper */}
            {/*Main layout*/}
            <h1 style={{textAlign: 'center', left: '45%', position: 'absolute', top: '15%'}}>New Volunteers List </h1>
            <table className="table align-middle mb-0 bg-white" style={{position : 'absolute', left: '35%', width: '50%', top: '30%'}}>
              <thead className="bg-light">
                <tr className="bg-white">
                  <th>#</th>
                  <th>Name</th>
                  <th>Volunter-ID</th>
                  <th>Applications</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td>
                  </td></tr><tr>
                  <th scope="row">1</th>
                  <td>Ali Rehman</td>
                  <td>V-002</td>
                  <td>
                    <button type="button" className="btn btn-primary">Check</button>
                  </td>
                </tr>
                <tr>
                  <th scope="row">2</th>
                  <td>Haleema Bibi</td>
                  <td>V-022</td>
                  <td>
                    <button type="button" className="btn btn-primary">Check</button>
                  </td>
                </tr>
                <tr>
                  <th scope="row">3</th>
                  <td>Selena</td>
                  <td>V-003</td>
                  <td>
                    <button type="button" className="btn btn-primary">Check</button>
                  </td>
                </tr>
                <tr>
                  <th scope="row">4</th>
                  <td>Kainat Jahan</td>
                  <td>V-056</td>
                  <td>
                    <button type="button" className="btn btn-primary">Check</button>
                  </td>
                </tr>
                <tr>
                  <th scope="row">5</th>
                  <td>Adnan Ahmad</td>
                  <td>V-003</td>
                  <td>
                    <button type="button" className="btn btn-primary">Check</button>
                  </td>
                </tr>
                <tr>
                  <th scope="row">6</th>
                  <td>Noman</td>
                  <td>V-007</td>
                  <td>
                    <button type="button" className="btn btn-primary">Check</button>
                  </td>
                </tr>
                <tr>
                  <th scope="row">7</th>
                  <td>Khan</td>
                  <td>V-090</td>
                  <td>
                    <button type="button" className="btn btn-primary">Check</button>
                  </td>
                </tr>
                <tr>
                  <th scope="row">8</th>
                  <td>Sadia</td>
                  <td>V-021 </td>
                  <td>
                    <button type="button" className="btn btn-primary">Check </button>
                  </td>
                </tr>
                <tr>
                  <th scope="row">9</th>
                  <td>Aman Shah</td>
                  <td>V-034</td>
                  <td>
                    <button type="button" className="btn btn-primary">Check</button>
                  </td>
                </tr>
                <tr>
                  <th scope="row">10</th>
                  <td>christina</td>
                  <td>V-004</td>
                  <td>
                    <button type="button" className="btn btn-primary">Check</button>
                  </td>
                </tr>
                <tr>
                  <th scope="row">11</th>
                  <td>Aaina Butt</td>
                  <td>V-034</td>
                  <td>
                    <button type="button" className="btn btn-primary">Check</button>
                  </td>
                </tr>
                <tr>
                  <th scope="row">12</th>
                  <td>Komal</td>
                  <td>V-078</td>
                  <td>
                    <button type="button" className="btn btn-primary">Check </button>
                  </td>
                </tr>
              </tbody>
            </table>
          </div>
         
      
    
  )
}

export default Newvolunteerlist