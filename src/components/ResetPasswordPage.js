import React from 'react'

export default function ResetPasswordPage() {
  return (
    <div className="container">
      <section style={{marginTop: "15%"}}>
        <div className="container shadow-4-strong"
            style={{width: "450px", height: "300px", borderRadius: "10px", border: "1px solid grey"}}>
            <h2 style={{textAlign: "center", marginTop: "5%"}}>Reset password</h2>
            <form style={{marginTop: "15%"}}>

              
                <div className="form-outline mb-4" style={{border: "1px solid grey"}}>
                    <input type="email" id="form4Example2" className="form-control" />
                    <label className="form-label" for="form4Example2">Email address</label>
                </div>


        
                <button type="submit" className="btn btn-primary btn-block mb-4">Reset Password</button>
            </form>
        </div>
    </section>
    </div>
  )
}
