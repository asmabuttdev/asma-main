import React from 'react'

function Newcases1() {
  return (
    <div>
       <header>
            {/* Intro settings */}
            <style dangerouslySetInnerHTML={{__html: "\n#intro {\n  /* Margin to fix overlapping fixed navbar */\n  margin-top: 100px;\n}\n" }} />
            {/* Navbar */}
            <nav className="navbar navbar-expand-lg navbar-light bg-white fixed-top">
              <div className="container-fluid">
                {/* Navbar brand */}
                <a className="navbar-brand" target="_blank" href="dhttps://mdbootstrap.com/docs/stanard/">
                  Social Welfare  
                </a>
                <button className="navbar-toggler" type="button" data-mdb-toggle="collapse" data-mdb-target="#navbarExample01" aria-controls="navbarExample01" aria-expanded="false" aria-label="Toggle navigation">
                  <i className="fas fa-bars" />
                </button>
                <div className="collapse navbar-collapse" id="navbarExample01" style={{marginLeft: '40%'}}>
                  <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                    <li className="nav-item active">
                      <a className="nav-link mx-2" aria-current="page" href="#intro">Home</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link mx-2" href="https://www.youtube.com/channel/UC5CF7mLQZhvx8O5GODZAhdA" rel="nofollow" target="_blank">Volunteer</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link mx-2" href="https://mdbootstrap.com/docs/standard/" target="_blank">Cases</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link mx-2" href="https://mdbootstrap.com/docs/standard/" target="_blank">Events</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link mx-2" href="https://mdbootstrap.com/docs/standard/" target="_blank">Apply Now</a>
                    </li>
                    <li className="nav-item">
                      <a className="nav-link mx-2" href="https://mdbootstrap.com/docs/standard/" target="_blank">Login</a>
                    </li>
                  </ul>
                  <button type="button" className="btn btn-primary">Donate</button>
                </div>
              </div></nav>
            {/* Navbar */}
          </header>
          {/*Main Navigation*/}
          <section className="row d-flex justify-content-center" style={{marginTop: '8%', width: '100%'}}>
            <div className="tab-content col-md-6 shadow-1-strong" style={{borderRadius: '10px'}}>
              <div className="container d-flex justify-content-center align-items-center mb-4">
                <div className="progresses">
                  <div className="container mt-2">
                    <h2>New Cases</h2>
                    <p>All the new cases are listed below:</p>            
                    <table className="table table-striped">
                      <thead>
                        <tr>
                          <th>Name</th>
                          <th>Case-ID</th>
                          <th>Applications</th>
                        </tr>
                      </thead>
                      <tbody>
                        <tr>
                          <td>John</td>
                          <td>001</td>
                          <td>
                            <div className="container mt-3">
                            <a href='/app8'>
                              <button type="button" className="btn btn-primary">view</button>
                           
                           </a> </div>
                          </td>
                        </tr>
                        <tr>
                          <td>John</td>
                          <td>001</td>
                          <td>
                            <div className="container mt-3">
                            <a href='/app8'>
                              <button type="button" className="btn btn-primary">view</button>
                          </a>
                            </div>
                          </td></tr>
                        <tr>
                          <td>Ali Rehman</td>
                          <td>022</td>
                          <td>
                            <div className="container mt-3">
                              <button type="button" className="btn btn-primary">view</button>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>Haleema bibi</td>
                          <td>021</td>
                          <td>
                            <div className="container mt-3">
                            <a href='/app8'>
                              <button type="button" className="btn btn-primary">view</button>
                            </a>
                            </div>
                          </td>
                        </tr>
                        <tr>
                          <td>Sadia bibi</td>
                          <td>025</td>
                          <td>
                            <div className="container mt-3">
                              <a href='/app8'>
                              <button type="button" className="btn btn-primary">view</button>
                          
                              </a>  </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div></div></div></section></div>
  
  )
}

export default Newcases1