import React from 'react'
import Header from './Header'

export default function Middlepage() {
  return (
    <>
   <Header/>
    <div >
      <section style={{marginTop: "0%"}}>

     
  
<div id="carouselBasicExample" class="carousel slide carousel-fade" data-mdb-ride="carousel">
 
  <div class="carousel-indicators">
    <button
      type="button"
      data-mdb-target="#carouselBasicExample"
      data-mdb-slide-to="0"
      class="active"
      aria-current="true"
      aria-label="Slide 1"
    ></button>
    <button
      type="button"
      data-mdb-target="#carouselBasicExample"
      data-mdb-slide-to="1"
      aria-label="Slide 2"
    ></button>
    <button
      type="button"
      data-mdb-target="#carouselBasicExample"
      data-mdb-slide-to="2"
      aria-label="Slide 3"
    ></button>
  </div>

  <div class="carousel-inner">
   
    <div class="carousel-item active">
      <img src="https://mdbcdn.b-cdn.net/img/Photos/Slides/img%20(15).webp" class="d-block w-100" alt="Sunset Over the City"/>
      

</div>  
    <div class="carousel-item">
      <img src="https://mdbcdn.b-cdn.net/img/Photos/Slides/img%20(22).webp" class="d-block w-100" alt="Canyon at Nigh"/>
      <div class="carousel-caption d-none d-md-block">
     

    </div>
    <div class="carousel-item">
      <img src="https://mdbcdn.b-cdn.net/img/Photos/Slides/img%20(23).webp" class="d-block w-100" alt="Cliff Above a Stormy Sea"/>
      
    </div>
  </div>

  <button class="carousel-control-prev" type="button" data-mdb-target="#carouselBasicExample" data-mdb-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Previous</span>
  </button>
  <button class="carousel-control-next" type="button" data-mdb-target="#carouselBasicExample" data-mdb-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="visually-hidden">Next</span>
  </button>
</div>

 </div>    
    </section>
      <section className="text-center container" style={{marginTop: "10%"}}>
            

            <div className="row">
            <div className="col-lg-4 col-md-12 mb-4">
                <div className="card">
                <div className="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                    <img src="https://mdbootstrap.com/img/new/standard/nature/184.jpg" alt='' className="img-fluid" />
                    <a href="#!">
                    <div className="mask" style={{backgroundColor: "rgba(251, 251, 251, 0.15)"}}></div>
                    </a>
                </div>
                <div className="card-body">
                    <h5 className="card-title">Post title</h5>
                    <p className="card-text">
                    Some quick example text to build on the card title and make up the bulk of the
                    card's content.
                    </p>
                    <a href="/paymethodoption" className="btn btn-primary">Donate</a>
                </div>
                </div>
            </div>

            <div className="col-lg-4 col-md-6 mb-4">
                <div className="card">
                <div className="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                    <img src="https://mdbootstrap.com/img/new/standard/nature/023.jpg" alt='' className="img-fluid" />
                    <a href="#!">
                    <div className="mask" style={{backgroundColor: "rgba(251, 251, 251, 0.15)"}}></div>
                    </a>
                </div>
                <div className="card-body">
                    <h5 className="card-title">Post title</h5>
                    <p className="card-text">
                    Some quick example text to build on the card title and make up the bulk of the
                    card's content.
                    </p>
                    <a href="/paymethodoption" className="btn btn-primary">Donate</a>
                </div>
                </div>
            </div>

            <div className="col-lg-4 col-md-6 mb-4">
                <div className="card">
                <div className="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                    <img src="https://mdbootstrap.com/img/new/standard/nature/111.jpg" alt='' className="img-fluid" />
                    <a href="#!">
                    <div className="mask" style={{backgroundColor: "rgba(251, 251, 251, 0.15)"}}></div>
                    </a>
                </div>
                <div className="card-body">
                    <h5 className="card-title">Post title</h5>
                    <p className="card-text">
                    Some quick example text to build on the card title and make up the bulk of the
                    card's content.
                    </p>
                    <a href="/paymethodoption" className="btn btn-primary">Donate</a>
                </div>
                </div>
            </div>
            </div>

            <div className="row">
            <div className="col-lg-4 col-md-12 mb-4">
                <div className="card">
                <div className="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                    <img src="https://mdbootstrap.com/img/new/standard/nature/184.jpg" alt='' className="img-fluid" />
                    <a href="#!">
                    <div className="mask" style={{backgroundColor: "rgba(251, 251, 251, 0.15)"}}></div>
                    </a>
                </div>
                <div className="card-body">
                    <h5 className="card-title">Post title</h5>
                    <p className="card-text">
                    Some quick example text to build on the card title and make up the bulk of the
                    card's content.
                    </p>
                    <a href="/paymethodoption" className="btn btn-primary">Donate</a>
                </div>
                </div>
            </div>

            <div className="col-lg-4 col-md-6 mb-4">
                <div className="card">
                <div className="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                    <img src="https://mdbootstrap.com/img/new/standard/nature/023.jpg" alt='' className="img-fluid" />
                    <a href="#!">
                    <div className="mask" style={{backgroundColor: "rgba(251, 251, 251, 0.15)"}}></div>
                    </a>
                </div>
                <div className="card-body">
                    <h5 className="card-title">Post title</h5>
                    <p className="card-text">
                    Some quick example text to build on the card title and make up the bulk of the
                    card's content.
                    </p>
                    <a href="/paymethodoption" className="btn btn-primary">Donate</a>
                </div>
                </div>
            </div>

            <div className="col-lg-4 col-md-6 mb-4">
                <div className="card">
                <div className="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                    <img src="https://mdbootstrap.com/img/new/standard/nature/111.jpg" alt='' className="img-fluid" />
                    <a href="#!">
                    <div className="mask" style={{backgroundColor: "rgba(251, 251, 251, 0.15)"}}></div>
                    </a>
                </div>
                <div className="card-body">
                    <h5 className="card-title">Post title</h5>
                    <p className="card-text">
                    Some quick example text to build on the card title and make up the bulk of the
                    card's content.
                    </p>
                    <a href="/paymethodoption" className="btn btn-primary">Donate</a>
                </div>
                </div>
            </div>
            </div>

            <div className="col-lg-4 col-md-6 mb-4">
            <div className="card">
                <div className="bg-image hover-overlay ripple" data-mdb-ripple-color="light">
                <img src="https://mdbootstrap.com/img/new/standard/nature/111.jpg" alt='' className="img-fluid" />
                <a href="#!">
                    <div className="mask" style={{backgroundColor: "rgba(251, 251, 251, 0.15)"}}></div>
                </a>
                </div>
                <div className="card-body">
                <h5 className="card-title">Post title</h5>
                <p className="card-text">
                    Some quick example text to build on the card title and make up the bulk of the
                    card's content.
                </p>
                <a href="/paymethodoption" className="btn btn-primary">Donate</a>
                </div>
            </div>
            </div>
           
        </section>
       
    </div>
    <footer className="text-center text-lg-start  text-muted" style={{backgroundColor: "#4d4d4f"}}>
   
   <section className="d-flex justify-content-center justify-content-lg-between p-4 border-bottom"
   style={{marginTop: "95%", color: "white"}}>

   <div className="me-5 d-none d-lg-block">
       <span>Get connected with us on social networks:</span>
   </div>

   <div>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-facebook-f"></i>
       </a>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-twitter"></i>
       </a>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-google"></i>
       </a>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-instagram"></i>
       </a>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-linkedin"></i>
       </a>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-github"></i>
       </a>
   </div>

   </section>

   <section style={{ height:"240px" }}>
   <div className="container text-center text-md-start mt-5" style={{backgroundColor: "#4d4d4f", color: "white"}}>
   
       <div className="row mt-3">
       
       <div className="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
       
           <h6 className="text-uppercase fw-bold mb-4">
           <i className="fas fa-gem me-3"></i>Social Welfare
           </h6>
           <p>
           Here you can use rows and columns to organize your footer content. Lorem ipsum
           dolor sit amet, consectetur adipisicing elit.
           </p>
       </div>
   
       <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
   
           <h6 className="text-uppercase fw-bold mb-4">
           Navigation
           </h6>
           <p>
           <a href="home" className="text-reset">Home</a>
           </p>
           <p>
           <a href="/contact" className="text-reset">Contact us</a>
           </p>
           <p>
           <a href="/app15" className="text-reset">About Us</a>
           </p>
           <p>
           <a href="/volunteer" className="text-reset">Volunteer</a>
           </p>
       </div>
       

       
       <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
           
           <h6 className="text-uppercase fw-bold mb-4">
           Explore
           </h6>
           <p>
           <a href="/eventdescrip" className="text-reset">Events</a>
           </p>
           <p>
           <a href="/allevent" className="text-reset">Projects</a>
           </p>
           <p>
           <a href="/rep" className="text-reset">Reports</a>
           </p>
           <p>
           <a href="/applynowrule" className="text-reset">Apply Now</a>
           </p>
       </div>
       

       
       <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
   
           <h6 className="text-uppercase fw-bold mb-4">
           Contact
           </h6>
           <p><i className="fas fa-home me-3"></i> Pakistan, NY 10012, US</p>
           <p>
           <i className="fas fa-envelope me-3"></i>
           info@example.com
           </p>
           <p><i className="fas fa-phone me-3"></i> + 01 234 567 88</p>
           <p><i className="fas fa-print me-3"></i> + 01 234 567 89</p>
       </div>
       
       </div>
   
   </div>
   
   </section>

   <section>
   <div class="text-center p-4 bg-dark" style={{backgrounColor: "#4d4d4f", color:"white"}}>
© 2021 Copyright:
<a class="text-reset fw-bold" href="https://mdbootstrap.com/">SocialWelfare.com</a>
</div>
   </section>    
</footer>
    </>
  )
}
