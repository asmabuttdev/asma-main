import React from 'react'
import Header from './Header'
import HomeSlider from './HomeSlider'
function GuideLines() {
  return (
    <div>

<Header/>
<HomeSlider/>
<div id="back-div" style={{position:"absolute",left:"2%",marginTop:"3%"}}>
<h3>Objective</h3>
<p>
The objective of this activity is to encourage and motivate the youth of Punjab to participate in volunteer work, build up a caring community, promote the positive values of self-fulfillment and enhance the sense of social belongings. Our country is facing numerous social, environmental, political and cultural issues, and we need volunteers to fill the gap among different strata of the society in general and to give appreciation to Young Volunteers in particular.
</p>
<h3>Objective</h3>
<p>
Volunteerism among Youth will be promoted through sharing of short, evidence based Video Documentaries of the volunteer work undertaken by individuals or group of individuals.
</p>
<h3>
Key Areas of Volunteerism
</h3>
<ul>
    <li>
    Community Projects
    </li>
    <li>
    Community Projects
    </li>
    <li>
    Community Projects
    </li>
    <li>
    Community Projects
    </li>
    <li>
    Community Projects
    </li>
    
</ul>
<h5>Who qualifies as a Youth Volunteer?</h5>
<p>
The Youth Volunteer programme is looking for motivated and talented youth between the ages of 15 and 29.
</p>
<h3>Eligibility</h3>
<p>
A volunteer should:

be a citizen of Pakistan and should fall in age bracket of 15 to 29 (Official age-group of Youth).
have valid CNIC or Form-B Certificate & father’s / guardian’s CNIC in case of under 18 years.
have valid proof of age / date of birth.
be an individual or group of individuals and not drawing any remuneration for the social work.
Volunteer work should have been undertaken in Punjab

</p>
<h3>Process / Steps</h3>
<p>Register at Punjab Youth Portal (Click Here)
A Confirmation Email will be sent at your provided email address upon successful registration
Upload your prepared video (as per criteria given below) on YouTube or Dailymotion.
Login (Click Here) to submit your entry and link to the uploaded video.
A Confirmation Email will be sent at your registered email address upon successful entry submission.
Note: Only qualified entries (as decided by the concerned committee) will be contacted within One Month of submission. Please do not call or email to inquire about the status of entry after submission.

Criteria for Videos Documentary</p>
<h3>
Criteria for Videos Documentary
</h3>
<p>
Documentary must start with the title (area of volunteer work) and brief introduction of the volunteer (maximum length- 30 seconds).
Documentary must have contents based on Social Volunteerism and its impact on the lives of people.
Documentary should be based on evidence of volunteer work highlighting the work of volunteer.
The submitted clips may preferably be 2-3 minutes long and it should not exceed 05 minutes. Submissions exceeding 05 minutes will be disqualified.
Videos is to be uploaded on YouTube or Dailymotion as one single file in high quality.
Short title of the entry and a descriptive text of maximum 1000 characters are to be entered in the online form at the time of Video submission.
Documentary should be free of any gender, sectarian or religious discrimination.
The documentary must be the original work of the volunteer.
The footage/photos should be from Pakistan only.
The video content must be in Urdu or English.
Participants must only use royalty free music/original background score for the film to avoid any copyright issues.s
</p>

</div>
<footer className="text-center text-lg-start  text-muted" style={{backgroundColor: "#4d4d4f"}}>
   
        <section className="d-flex justify-content-center justify-content-lg-between p-4 border-bottom"
        style={{marginTop: "80%", color: "white"}}>
    
        <div className="me-5 d-none d-lg-block">
            <span>Get connected with us on social networks:</span>
        </div>
    
        <div>
            <a href="/" className="me-4 text-reset">
            <i className="fab fa-facebook-f"></i>
            </a>
            <a href="/" className="me-4 text-reset">
            <i className="fab fa-twitter"></i>
            </a>
            <a href="/" className="me-4 text-reset">
            <i className="fab fa-google"></i>
            </a>
            <a href="/" className="me-4 text-reset">
            <i className="fab fa-instagram"></i>
            </a>
            <a href="/" className="me-4 text-reset">
            <i className="fab fa-linkedin"></i>
            </a>
            <a href="/" className="me-4 text-reset">
            <i className="fab fa-github"></i>
            </a>
        </div>
    
        </section>

        <section style={{ height:"240px" }}>
        <div className="container text-center text-md-start mt-5" style={{backgroundColor: "#4d4d4f", color: "white"}}>
        
            <div className="row mt-3">
            
            <div className="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
            
                <h6 className="text-uppercase fw-bold mb-4">
                <i className="fas fa-gem me-3"></i>Social Welfare
                </h6>
                <p>
                Here you can use rows and columns to organize your footer content. Lorem ipsum
                dolor sit amet, consectetur adipisicing elit.
                </p>
            </div>
        
            <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
        
                <h6 className="text-uppercase fw-bold mb-4">
                Navigation
                </h6>
                <p>
                <a href="#!" className="text-reset">Angular</a>
                </p>
                <p>
                <a href="#!" className="text-reset">React</a>
                </p>
                <p>
                <a href="#!" className="text-reset">Vue</a>
                </p>
                <p>
                <a href="#!" className="text-reset">Laravel</a>
                </p>
            </div>
            

            
            <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
                
                <h6 className="text-uppercase fw-bold mb-4">
                Explore
                </h6>
                <p>
                <a href="#!" className="text-reset">Pricing</a>
                </p>
                <p>
                <a href="#!" className="text-reset">Settings</a>
                </p>
                <p>
                <a href="#!" className="text-reset">Orders</a>
                </p>
                <p>
                <a href="#!" className="text-reset">Help</a>
                </p>
            </div>
            

            
            <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
        
                <h6 className="text-uppercase fw-bold mb-4">
                Contact
                </h6>
                <p><i className="fas fa-home me-3"></i> New York, NY 10012, US</p>
                <p>
                <i className="fas fa-envelope me-3"></i>
                info@example.com
                </p>
                <p><i className="fas fa-phone me-3"></i> + 01 234 567 88</p>
                <p><i className="fas fa-print me-3"></i> + 01 234 567 89</p>
            </div>
            
            </div>
        
        </div>
        
        </section>

        <section>
        <div class="text-center p-4 bg-dark" style={{backgrounColor: "#4d4d4f", color:"white"}}>
    © 2021 Copyright:
    <a class="text-reset fw-bold" href="https://mdbootstrap.com/">MDBootstrap.com</a>
  </div>
        </section>    
  </footer>
    </div>
  )
}

export default GuideLines