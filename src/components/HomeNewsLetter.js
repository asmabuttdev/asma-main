import React from 'react'

export default function HomeNewsLetter() {
  return (
    <div className='container'>
        <section style={{marginTop: "50%"}}>

<div className="content shadow-4-strong col-md-12 mb-4" 
        style={{height: "300px", backgroundColor: "#0C56D0", borderRadius: "40px"}}>
  <div className="tab-pane fade show active" id="pills-login" role="tabpanel" aria-labelledby="tab-login">
    <form>
      <h2 style={{color: "white", textAlign: "center", lineHeight: "100px"}}>NewsLetter</h2>
      <div className="text-center mb-3">



         <div className="form-outline form-control" style={{width: "500px", border: "1px solid black",
                                     borderRadius: "4px", margin: "auto",
                                     marginTop: "3%", backgroundColor: "white" }}>
            <div className="form-outline">
                <input type="text" id="form12" className="form-control" />
                <label className="form-label" htmlFor="form12">Email</label>
            </div>
        </div>
    </div>

      
        <button type="submit" className="btn btn-primary btn-block mb-4" style={{width: "500px",  marginTop: "3%", margin: "3% auto auto auto",
            color: "#0C56D0", backgroundColor: "white", fontWeight: "bold", fontSize: "15px"}}>

          Subscribe
        </button>


    </form>
</div>
</div>
</section>
    </div>

  )
}
