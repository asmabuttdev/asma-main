import React from 'react'

function Approvedcases1() {
  return (
    <div>

<div>

<div> <section className="row d-flex justify-content-center mb-4" style={{marginTop: "8%", width: "100%"}}>
<div className="tab-content col-10 shadow-2-strong" style={{borderRadius: "10px"}}>
  <div className="row mx-2 my-5" >
      <div className="col-3 shadow-1-strong" style={{height: "780px", borderRadius: "30px"}}>
      
        <div className="mt-4 text-center">
          <h4>Dashboard</h4>
        </div>
      
      <div className="text-center mt-4 mb-3" >
          
          <img  className="rounded-circle shadow-2-strong" width="150px" height="150px" alt="avatar" />    
          
          </div>
     
      <div className="text-center" style={{marginBottom: "25%"}}>
          
          <h6>Asma Butt</h6>   
          
          </div>
      
        <div
          className="nav flex-column nav-pills text-center"
          id="v-pills-tab"
          role="tablist"
          aria-orientation="vertical" >
          <a
            className="nav-link active"
            id="v-pills-home-tab"
            data-mdb-toggle="pill"
            href="#v-pills-home"
            role="tab"
            aria-controls="v-pills-home"
            aria-selected="true"
            >Personal Info</a
          >
          <a
            className="nav-link active"
            id="v-pills-home-tab"
            data-mdb-toggle="pill"
            href="#v-pills-home"
            role="tab"
            aria-controls="v-pills-home"
            aria-selected="true"
            >Recent Cases</a
          >
          <a
            className="nav-link"
            id="v-pills-profile-tab"
            
            href="/app11"
        
            aria-controls="v-pills-profile"
            aria-selected="false"
            >New Cases</a>
          <a
            className="nav-link"
            id="v-pills-messages-tab"
           
            href="/spamcase"
           
            aria-controls="v-pills-messages"
            aria-selected="false"
            >Spam cases</a
          >
          <a
            className="nav-link"
            id="v-pills-setting-tab"
            
            href="/app9"
           
            aria-controls="v-pills-setting"
            aria-selected="false"
            >Approved Cases</a
          >
          <a
            className="nav-link"
           
            href="/app11"
          
            aria-controls="v-pills-setting"
            aria-selected="false"
            >New Volunteer</a
          >
          <a
            className="nav-link"
            id="v-pills-setting-tab"
            data-mdb-toggle="pill"
            href="#v-pills-setting"
            role="tab"
            aria-controls="v-pills-setting"
            aria-selected="false"
            >Spam Volunteer</a
          >
          <a
            className="nav-link"
            id="v-pills-setting-tab"
            data-mdb-toggle="pill"
            href="#v-pills-setting"
            role="tab"
            aria-controls="v-pills-setting"
            aria-selected="false"
            >Approved Volunteer</a
          >
          <div class="btn-group">
<button type="button" class="btn btn-primary dropdown-toggle" data-mdb-toggle="dropdown" aria-expanded="false">
Volunteer Reference
</button>
<ul class="dropdown-menu">
<li><button class="dropdown-item" type="button">Assign Case</button></li>
<li><button class="dropdown-item" type="button">All volunteeers</button></li>

</ul>
</div>
          
       

          <div class="btn-group">
<button type="button" class="btn btn-primary dropdown-toggle" data-mdb-toggle="dropdown" aria-expanded="false">
Projects
</button>
<ul class="dropdown-menu">
<li><button class="dropdown-item" type="button">Add Projects</button></li>
<li><button class="dropdown-item" type="button">Previous Project</button></li>

</ul>
</div>
          
          <div class="btn-group" style={{position:"absolute",top:"129%"}}>
<button type="button" class="btn btn-primary dropdown-toggle" data-mdb-toggle="dropdown" aria-expanded="false">
Categories
</button>
<ul class="dropdown-menu">
<li><button class="dropdown-item" type="button">Education Case</button></li>
<li><button class="dropdown-item" type="button">Monthly stipend</button></li>
<li><button class="dropdown-item" type="button">Marriage grant</button></li>
<li><button class="dropdown-item" type="button">Medical Allownace</button></li>
</ul>
</div>
<div className="text-center mb-3" style={{marginTop: "50%"}}>
          <button className="btn btn-primary" style={{width: "120px"}}>
              Logout
          </button>
      </div>
        </div>
       
       
      </div>
    
      <div className="col-9">


   
        <div className="tab-content" id="v-pills-tabContent">
          <div
            className="tab-pane fade show active"
            id="v-pills-home"
            role="tabpanel"
            aria-labelledby="v-pills-home-tab"
          >
          <div className="mt-4 text-center">
            <h4>Approved Case</h4>
          </div>
            
          <form className="my-5 mx-4">
           
            
              <div className="mb-4 " style={{width: "48.5%"}}>
              <table class="table align-middle mb-0 bg-white">
<thead class="bg-light">
<tr>
  <th>#</th>
  <th>Case Identity</th>
  <th>Category</th>
  <th>Status</th>
  <th>View</th>
</tr>
</thead>
<tbody>
<tr>
  <td>
   
      
      <div className="ms-1">
        <p className="fw-bold mb-1">1</p>
       
     
    </div>
  </td>
  <td>
    <p className="fw-normal mb-1">Education case</p>
   
  </td>
  <td>
    <span className="badge badge-success rounded-pill d-inline">Active</span>
  </td>
  <td>Senior</td>
  <td>
    <button type="button" className="btn btn-link btn-sm btn-rounded">
      Accept
    </button>
    <button type="button" className="btn btn-link btn-sm btn-rounded">
      Deny
    </button>
  </td>
</tr>
<tr>
  <td>
    <div className="d-flex align-items-center">
      <img
          src="https://mdbootstrap.com/img/new/avatars/8.jpg"
          alt=""
          style={{width: "45px", height: "45px"}}
          className="rounded-circle"
          />
      <div className="ms-3">
        <p className="fw-bold mb-1">John Doe</p>
        <p className="text-muted mb-0">john.doe@gmail.com</p>
      </div>
    </div>
  </td>
  <td>
    <p className="fw-normal mb-1">Software engineer</p>
    <p className="text-muted mb-0">IT department</p>
  </td>
  <td>
    <span className="badge badge-success rounded-pill d-inline">Active</span>
  </td>
  <td>Senior</td>
  <td>
  <button type="button" className="btn btn-link btn-sm btn-rounded">
      Accept
    </button>
    <button type="button" className="btn btn-link btn-sm btn-rounded">
      Deny
    </button>
  </td>
</tr>
<tr>
  <td>
    <div className="d-flex align-items-center">
      <img
          src="https://mdbootstrap.com/img/new/avatars/8.jpg"
          alt=""
          style={{width: "45px", height: "45px"}}
          className="rounded-circle"
          />
      <div className="ms-3">
        <p className="fw-bold mb-1">John Doe</p>
        <p className="text-muted mb-0">john.doe@gmail.com</p>
      </div>
    </div>
  </td>
  <td>
    <p className="fw-normal mb-1">Software engineer</p>
    <p className="text-muted mb-0">IT department</p>
  </td>
  <td>
    <span className="badge badge-success rounded-pill d-inline">Active</span>
  </td>
  <td>Senior</td>
  <td>
    <button type="button" className="btn btn-link btn-sm btn-rounded">
      Accept
    </button>
    <button type="button" className="btn btn-link btn-sm btn-rounded">
      Deny
    </button>
  </td>
</tr>

<tr>
  <td>
    <div className="d-flex align-items-center">
      <img
          src="https://mdbootstrap.com/img/new/avatars/6.jpg"
          className="rounded-circle"
          alt=""
          style={{width: "45px", height: "45px"}}
          />
      <div className="ms-3">
        <p className="fw-bold mb-1">Alex Ray</p>
        <p className="text-muted mb-0">alex.ray@gmail.com</p>
      </div>
    </div>
  </td>
  <td>
    <p className="fw-normal mb-1">Consultant</p>
    <p className="text-muted mb-0">Finance</p>
  </td>
  <td>
    <span className="badge badge-primary rounded-pill d-inline"
          >Onboarding</span
      >
  </td>
  <td>Junior</td>
  <td>
  <button type="button" className="btn btn-link btn-sm btn-rounded">
      Accept
    </button>
    <button type="button" className="btn btn-link btn-sm btn-rounded">
      Deny
    </button>
  </td>
</tr>
<tr>
  <td>
    <div className="d-flex align-items-center">
      <img
          src="https://mdbootstrap.com/img/new/avatars/7.jpg"
          className="rounded-circle"
          alt=""
          style={{width: "45px", height: "45px"}}
          />
      <div className="ms-3">
        <p className="fw-bold mb-1">Kate Hunington</p>
        <p className="text-muted mb-0">kate.hunington@gmail.com</p>
      </div>
    </div>
  </td>
  <td>
    <p className="fw-normal mb-1">Designer</p>
    <p className="text-muted mb-0">UI/UX</p>
  </td>
  <td>
    <span className="badge badge-warning rounded-pill d-inline">Awaiting</span>
  </td>
  <td>Senior</td>
  <td>
    <button
            type="button"
            className="btn btn-link btn-rounded btn-sm fw-bold"
            data-mdb-ripple-color="dark"
            >
      Edit
    </button>
  </td>
</tr>
</tbody>
</table>


</div>
</form>
          </div>
          
        </div>
        
      </div>
    </div>
</div>
</section>
</div>

</div>
    </div>
  )
}

export default Approvedcases1