import React from 'react'
import './foo.css';


import Header from '../components/Header';

function Dashboard() {
  return (
 
<div>
    <Header/>
    <section className="row d-flex justify-content-center mb-4" style={{marginTop: "8%", width: "100%"}}>
    <div className="tab-content col-10 shadow-2-strong" style={{borderRadius: "10px"}}>
      <div className="row mx-2 my-5" >
          <div className="col-3 shadow-1-strong" style={{height: "780px", borderRadius: "30px"}}>
          
            <div className="mt-4 text-center">
              <h4>Dashboard</h4>
            </div>
          
          <div className="text-center mt-4 mb-3" >
              
              <img src="./img/school-india-children-ahmedabad.jpg" className="rounded-circle shadow-2-strong" width="150px" height="150px" alt="avatar" />    
              
              </div>
         
          <div className="text-center" style={{marginBottom: "25%"}}>
              
              <h6>Asma Butt</h6>   
              
              </div>
          
            <div
              className="nav flex-column nav-pills text-center"
              id="v-pills-tab"
              role="tablist"
              aria-orientation="vertical" >
              <a
                className="nav-link active"
                id="v-pills-home-tab"
                data-mdb-toggle="pill"
                href="#v-pills-home"
                role="tab"
                aria-controls="v-pills-home"
                aria-selected="true"
                >Personal Info</a
              >
              <a
                className="nav-link"
                id="v-pills-profile-tab"
                data-mdb-toggle="pill"
                href="#v-pills-newcase"
                role="tab"
                aria-controls="v-pills-profile"
                aria-selected="false"
                >New Cases</a>
              <a
                className="nav-link"
                id="v-pills-messages-tab"
                data-mdb-toggle="pill"
                href="#v-pills-spamcase"
                role="tab"
                aria-controls="v-pills-messages"
                aria-selected="false"
                >Spam Case</a
              >
              <a
                className="nav-link"
                id="v-pills-setting-tab"
                data-mdb-toggle="pill"
                href="#v-pills-accase"
                role="tab"
                aria-controls="v-pills-setting"
                aria-selected="false"
                >Accepted Case</a
              >
              <a
                className="nav-link"
                id="v-pills-setting-tab"
                data-mdb-toggle="pill"
                href="#v-pills-setting"
                role="tab"
                aria-controls="v-pills-setting"
                aria-selected="false"
                >Settings</a
              >
              
            </div>
            <div className="text-center mb-3" style={{marginTop: "50%"}}>
              <button className="btn btn-primary" style={{width: "120px"}}>
                  Logout
              </button>
          </div>
           
          </div>
        
          <div className="col-9">
  
  
       
            <div className="tab-content" id="v-pills-tabContent">
              <div
                className="tab-pane fade show active"
                id="v-pills-home"
                role="tabpanel"
                aria-labelledby="v-pills-home-tab"
              >
              <div className="mt-4 text-center">
                <h4>Personal Information</h4>
              </div>
                
              <form className="my-5 mx-4">
               
                
                  <div className="mb-4 " style={{width: "48.5%"}}>
                    <div className="form-outline">
                      <input style={{border:"1px solid black"}} type="text" id="form6Example1" className="form-control" />
                      <label className="form-label" htmlFor="form6Example1">UserName</label>
                    </div>
                  </div>
                
              
                
                <div className="row mb-4">
                  <div className="col">
                <div className="form-outline">
                  <input style={{border:"1px solid black"}} type="text" id="form6Example3" className="form-control" />
                  <label className="form-label" htmlFor="form6Example3">First Name</label>
                </div>
              </div>
              <div className="col">
                <div className="form-outline">
                  <input style={{border:"1px solid black"}} type="text" id="form6Example3" className="form-control" />
                  <label className="form-label" htmlFor="form6Example3">Last Name</label>
                </div>
              </div>
              </div>
              
               
                <div className="form-outline mb-4" style={{width: "48.5%"}}>
                  <input style={{border:"1px solid black"}} type="text" id="form6Example4" className="form-control" />
                  <label className="form-label" htmlFor="form6Example4">CNIC</label>
                </div>
             
                <div className="row mb-4">
                  <div className="col">
                <div className="form-outline">
                  <input style={{border:"1px solid black"}} type="email" id="form6Example5" className="form-control" />
                  <label className="form-label" htmlFor="form6Example5">Primary PhoneNo</label>
                </div>
              </div>
              <div className="col">
                <div className="form-outline">
                  <input style={{border:"1px solid black"}} type="email" id="form6Example5" className="form-control" />
                  <label className="form-label" htmlFor="form6Example5">Secondary PhoneNo</label>
                </div>
              </div>
              </div>
              
               
                
                <div className="form-outline mb-4">
                  <input style={{border:"1px solid black"}} type="number" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">Current Address</label>
                </div>
                <div className="form-outline mb-4">
                  <input style={{border:"1px solid black"}} type="number" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">Permanent Address</label>
                </div>
              
              
                
                <div className="row mb-4">
                  <div className="col">
                <div className="form-outline mb-4">
                  <input style={{border:"1px solid black"}} type="text" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">Province</label>
                </div>
              </div>
              <div className="col">
                <div className="form-outline mb-4">
                  <input style={{border:"1px solid black"}} type="text" id="form6Example6" className="form-control" />
                  <label className="form-label" htmlFor="form6Example6">City</label>
                </div>
              </div>
            </div>
              
              
                <div className="text-center mb-3" style={{marginTop: "24%"}}>
                <button type="submit" className="btn btn-primary mb-4 ">Save</button>
              </div>
              </form>
  
              </div>
  
            
              <div
                className="tab-pane fade"
                id="v-pills-newcase"
                role="tabpanel"
                aria-labelledby="v-pills-profile-tab"
              >
                
             
                
              <form className="my-5">
              <div className="mt-4 text-center">
                <h4>New Cases</h4>
              </div>
              <div className="mb-4 " style={{width: "48.5%",position:"absolute",left:"35%"}}>
             
                  <table class="table align-middle mb-0 bg-white"
                   >
  <thead class="bg-light">
  <tr>
      <th style={{color:"black"}}>Case Id</th>
      <th style={{color:"black"}}>CaseTitle</th>
      <th style={{color:"black"}}>Approval</th>
      <th style={{color:"black"}}>Denied</th>
      <th style={{color:"black"}}>View</th>
    
    </tr>
    </thead>
    <tbody>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Denny
        </button>
        
      </td>
   
      <td>
        <a href='/app8'>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
      <a href='/app8'>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
        </a>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
      <a href='/app8'>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
        </a>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
        
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
      
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
      
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
   
    
   
    
     
       
  </tbody>
</table>

</div>
                 </form>
                 </div>
                 <div
                className="tab-pane fade"
                id="v-pills-accase"
                role="tabpanel"
                aria-labelledby="v-pills-profile-tab"
              >
                
             
                
              <form className="my-5">
              <div className="mt-4 text-center">
                <h4>Accepted Cases</h4>
              </div>
              <div className="mb-4 " style={{width: "48.5%",position:"absolute",left:"35%"}}>
             
                  <table class="table align-middle mb-0 bg-white"
                   >
  <thead class="bg-light">
  <tr>
      <th style={{color:"black"}}>Case Id</th>
      <th style={{color:"black"}}>CaseTitle</th>
      <th style={{color:"black"}}>Approval</th>
      <th style={{color:"black"}}>Denied</th>
      <th style={{color:"black"}}>View</th>
    
    </tr>
    </thead>
    <tbody>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Denny
        </button>
        
      </td>
   
      <td>
        <a href='/app8'>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
      <a href='/app8'>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
        </a>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
      <a href='/app8'>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
        </a>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
        
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
      
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
      
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
   
    
   
    
     
       
  </tbody>
</table>

</div>
                 </form>
                 </div>
           <div
          className="tab-pane fade"
                id="v-pills-spamcase"
                role="tabpanel"
                aria-labelledby="v-pills-profile-tab"
              >
                
             
                
              <form className="my-5">
              <div className="mt-4 text-center">
                <h4>spam Cases</h4>
              </div>
              <div className="mb-4 " style={{width: "48.5%",position:"absolute",left:"35%"}}>
             
                  <table class="table align-middle mb-0 bg-white"
                   >
  <thead class="bg-light">
  <tr>
      <th style={{color:"black"}}>Case Id</th>
      <th style={{color:"black"}}>CaseTitle</th>
      <th style={{color:"black"}}>Approval</th>
      <th style={{color:"black"}}>Denied</th>
      <th style={{color:"black"}}>View</th>
    
    </tr>
    </thead>
    <tbody>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Denny
        </button>
        
      </td>
   
      <td>
        <a href='/app8'>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
        </a>
      </td>
    </tr>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
      <a href='/app8'>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
        </a>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
      <a href='/app8'>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
        </a>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
        
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
      
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
      
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
   
    
   
    
     
       
  </tbody>
</table>

    
    
                  
                  </div>
                
                
                  </form>
                 
               
               
                  </div>

{/* setting */}



<div
          className="tab-pane fade"
                id="v-pills-setting"
                role="tabpanel"
                aria-labelledby="v-pills-profile-tab"
              >
                
             
                
              <form className="my-5">
              <div className="mt-4 text-center">
                <h4>Privacy and Setting</h4>
              </div>
              <div className="mb-4 " style={{width: "48.5%",position:"absolute",left:"35%"}}>
             
             
                      <div className="mb-4 " style={{ width: "48.5%" }}>
                        <div
                          className="shadow-sm p-3  bg-white rounded"
                          style={{ width: "200%" }}
                        >
                          <h5>Language </h5>
                          <div
                            class="dropdown"
                            style={{
                              position: "absolute",
                              top: "3%",
                              left: "75%",
                            }}
                          >
                            <a
                              class="btn btn-primary dropdown-toggle"
                              href="#"
                              role="button"
                              id="dropdownMenuLink"
                              data-mdb-toggle="dropdown"
                              aria-expanded="false"
                            >
                              Change
                            </a>

                            <ul
                              class="dropdown-menu"
                              aria-labelledby="dropdownMenuLink"
                            >
                              <li>
                                <a class="dropdown-item" href="#">
                                  English
                                </a>
                              </li>
                              <li>
                                <a class="dropdown-item" href="#">
                                  Urdu
                                </a>
                              </li>
                            </ul>
                          </div>
                          <hr/>
                          <h5>Color Mode </h5>

                          <div
                            class="dropdown"
                            style={{
                              position: "absolute",
                              top: "20%",
                              left: "75%",
                            }}
                          >
                            <a
                              class="btn btn-primary dropdown-toggle"
                              href="#"
                              role="button"
                              id="dropdownMenuLink"
                              data-mdb-toggle="dropdown"
                              aria-expanded="false"
                            >
                              Change
                            </a>

                            <ul
                              class="dropdown-menu"
                              aria-labelledby="dropdownMenuLink"
                            >
                              <li>
                                <a class="dropdown-item" href="#">
                                  English
                                </a>
                              </li>
                              <li>
                                <a class="dropdown-item" href="#">
                                  Urdu
                                </a>
                              </li>
                            </ul>
                          </div>
                          <hr/>
                          <h5>Account Status </h5>

                          <div
                            class="dropdown"
                            style={{
                              position: "absolute",
                              top: "40%",
                              left: "75%"
                            }}
                          >
                            <a
                              class="btn btn-primary dropdown-toggle"
                              href="#"
                              role="button"
                              id="dropdownMenuLink"
                              data-mdb-toggle="dropdown"
                              aria-expanded="false"
                            >
                              Change
                            </a>

                            <ul
                              class="dropdown-menu"
                              aria-labelledby="dropdownMenuLink"
                            >
                              <li>
                                <a class="dropdown-item" href="#">
                                  Activate
                                </a>
                              </li>
                              <li>
                                <a class="dropdown-item" href="#">
                                  Deactivate
                                </a>
                              </li>
                            </ul>
                          </div>
                          <hr/>
                          <h5>Profile Setting </h5>

                          <div
                            class="dropdown"
                            style={{
                              position: "absolute",
                              top: "60%",
                              left: "75%",
                            }}
                          >
                            <a
                              class="btn btn-primary dropdown-toggle"
                              href="#"
                              role="button"
                              id="dropdownMenuLink"
                              data-mdb-toggle="dropdown"
                              aria-expanded="false"
                            >
                              Change
                            </a>

                            <ul
                              class="dropdown-menu"
                              aria-labelledby="dropdownMenuLink"
                            >
                              <li>
                                <a class="dropdown-item" href="#">
                                  Hide/Private
                                </a>
                              </li>
                              <li>
                                <a class="dropdown-item" href="#">
                                  Show/Public
                                </a>
                              </li>
                            </ul>
                          </div>
                          <hr/>
                          
                         
                          <h5>Profile Information (Name) </h5>

                          <div
                            class="dropdown"
                            style={{
                              position: "absolute",
                              top: "79%",
                              left: "75%",
                            }}
                          >
                            <a
                              class="btn btn-primary dropdown-toggle"
                              href="#"
                              role="button"
                              id="dropdownMenuLink"
                              data-mdb-toggle="dropdown"
                              aria-expanded="false"
                            >
                              Change
                            </a>

                           
                          </div>
                        </div>
                      </div>
                  


    
    
                  
                  </div>
                
                
                  </form>
                 
               
               
                  </div>



                  </div>
                  </div>
                  </div>
                  </div>
                  </section>
                  <footer className="text-center text-lg-start  text-muted" style={{backgroundColor: "#4d4d4f"}}>
   
   <section className="d-flex justify-content-center justify-content-lg-between p-4 border-bottom"
   style={{marginTop: "15%", color: "white"}}>

   <div className="me-5 d-none d-lg-block">
       <span>Get connected with us on social networks:</span>
   </div>

   <div>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-facebook-f"></i>
       </a>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-twitter"></i>
       </a>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-google"></i>
       </a>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-instagram"></i>
       </a>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-linkedin"></i>
       </a>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-github"></i>
       </a>
   </div>

   </section>

   <section style={{ height:"240px" }}>
   <div className="container text-center text-md-start mt-5" style={{backgroundColor: "#4d4d4f", color: "white"}}>
   
       <div className="row mt-3">
       
       <div className="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
       
           <h6 className="text-uppercase fw-bold mb-4">
           <i className="fas fa-gem me-3"></i>Social Welfare
           </h6>
           <p>
           Here you can use rows and columns to organize your footer content. Lorem ipsum
           dolor sit amet, consectetur adipisicing elit.
           </p>
       </div>
   
       <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
   
           <h6 className="text-uppercase fw-bold mb-4">
           Navigation
           </h6>
           <p>
           <a href="home" className="text-reset">Home</a>
           </p>
           <p>
           <a href="/contact" className="text-reset">Contact us</a>
           </p>
           <p>
           <a href="/app15" className="text-reset">About Us</a>
           </p>
           <p>
           <a href="/volunteer" className="text-reset">Volunteer</a>
           </p>
       </div>
       

       
       <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
           
           <h6 className="text-uppercase fw-bold mb-4">
           Explore
           </h6>
           <p>
           <a href="/approv" className="text-reset">Events</a>
           </p>
           <p>
           <a href="/allevent" className="text-reset">Projects</a>
           </p>
           <p>
           <a href="/rep" className="text-reset">Reports</a>
           </p>
           <p>
           <a href="/applynowrule" className="text-reset">Apply Now</a>
           </p>
       </div>
       

       
       <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
   
           <h6 className="text-uppercase fw-bold mb-4">
           Contact
           </h6>
           <p><i className="fas fa-home me-3"></i> Pakistan, NY 10012, US</p>
           <p>
           <i className="fas fa-envelope me-3"></i>
           info@example.com
           </p>
           <p><i className="fas fa-phone me-3"></i> + 01 234 567 88</p>
           <p><i className="fas fa-print me-3"></i> + 01 234 567 89</p>
       </div>
       
       </div>
   
   </div>
   
   </section>

   <section>
   <div class="text-center p-4 bg-dark" style={{backgrounColor: "#4d4d4f", color:"white"}}>
© 2021 Copyright:
<a class="text-reset fw-bold" href="https://mdbootstrap.com/">SocialWelfare.com</a>
</div>
   </section>    
</footer>
                  </div>
                  
         
        
  
           
          
 
  
   
   
  )
}

export default Dashboard