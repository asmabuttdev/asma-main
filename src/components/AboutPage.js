import React from 'react'

export default function AboutPage() {
  return (
    <div classNameName='container'>
      <h1 style={{color: "#0C56D0", marginTop: "10%", textAlign: "center"}}>About Us</h1>
    <section className="container">

        <div className="container-fluid gx-0" style={{height: "500px", marginTop: "5%"}}>
            <div style={{height: "500px", width: "50%", float: "left"}}>
                <img src="./img/1.jpg" alt='' width="100%" height="500px" className="image-responsive" />
            </div>
            <div className="container"
                style={{height: "500px", width: "50%", float: "right",backgroundColor: "rgb(218, 218, 218)"}}>
                <h1 style={{textAlign: "left", marginLeft: "5%", marginTop: "7%"}}>What We Want</h1>
                <p style={{textAlign: "justify", marginLeft: "5%", marginRight: "7%"}}>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rem nulla beatae consequatur, at natus es
                    t, suscipit hic totam esse vero eaque a nihil culpa molestiae? Temporibus mollitia similique unde
                    blanditiis?
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rem nulla beatae consequatur, at natus es
                    t, suscipit hic totam esse vero eaque a nihil culpa molestiae? Temporibus mollitia similique unde
                    blanditiis?
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rem nulla beatae consequatur, at natus es
                    t, suscipit hic totam esse vero eaque a nihil culpa molestiae? Temporibus mollitia similique unde
                    blanditiis?
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rem nulla beatae consequatur, at natus es
                    t, suscipit hic totam esse vero eaque a nihil culpa molestiae? Temporibus mollitia similique unde
                    blanditiis?
                </p>
                <button className="btn btn-primary mx-5 shadow-4-strong" style={{height: "50px"}}>Donate Now</button>

            </div>
        </div>
    </section>
    <h1 style={{color: "#0C56D0", marginTop: "10%", textAlign: "center"}}>Our Vision</h1>
    <section className="container my-5">

        <div className="container-fluid gx-0" style={{height: "500px", marginTop: "5%"}}>
            <div style={{height: "500px", width: "50%", float: "right"}}>
                <img src="./img/1.jpg" alt='' width="100%" height="500px" className="image-responsive" />
            </div>
            <div className="container"
                style={{height: "500px", width: "50%", float: "left",backgroundColor: "rgb(218, 218, 218)"}} >
                <h1 style={{textAlign: "left", marginLeft: "5%", marginTop: "7%"}}>What We Want</h1>
                <p style={{textAlign: "justify", marginLeft: "5%", marginRight: "7%"}}>
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rem nulla beatae consequatur, at natus es
                    t, suscipit hic totam esse vero eaque a nihil culpa molestiae? Temporibus mollitia similique unde
                    blanditiis?
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rem nulla beatae consequatur, at natus es
                    t, suscipit hic totam esse vero eaque a nihil culpa molestiae? Temporibus mollitia similique unde
                    blanditiis?
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rem nulla beatae consequatur, at natus es
                    t, suscipit hic totam esse vero eaque a nihil culpa molestiae? Temporibus mollitia similique unde
                    blanditiis?
                    Lorem ipsum, dolor sit amet consectetur adipisicing elit. Rem nulla beatae consequatur, at natus es
                    t, suscipit hic totam esse vero eaque a nihil culpa molestiae? Temporibus mollitia similique unde
                    blanditiis?
                </p>
                <button className="btn btn-primary mx-5 shadow-4-strong" style={{height: "50px"}}>Donate Now</button>

            </div>
        </div>
    </section>

    <div className="container" style={{textAlign: "center", padding: "10px", marginTop: "10%"}}>
        <button className="btn btn-primary shadow-4-strong" style={{height: "50px", fontSize: "20px"}}>
            Our Projects
        </button>

        <button className="btn btn-primary shadow-4-strong" style={{height: "50px", fontSize: "20px", marginLeft: "30px"}} >
            Want to Donate
        </button>

        <button className="btn btn-primary shadow-4-strong" style={{height: "50px", fontSize: "20px", marginLeft: "30px"}}>
            Latest Events
        </button>

    </div>
    </div>
  )
}
