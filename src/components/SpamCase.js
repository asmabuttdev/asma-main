import React from 'react'
import Header from './Header'
import './foo.css';
function SpamCase() {
  return (
    <>
     <Header/>
     <div>
         <section className="row d-flex justify-content-center mb-4" style={{marginTop: "8%", width: "100%"}}>
      <div className="tab-content col-10 shadow-2-strong" style={{borderRadius: "10px"}}>
      <div className="row mx-2 my-5" >
          <div className="col-3 shadow-1-strong" style={{height: "780px", borderRadius: "30px"}}>
          
            <div className="mt-4 text-center">
              <h4>Dashboard</h4>
            </div>
          
          <div className="text-center mt-4 mb-3" >
              
              <img src="./img/img1.jpg" className="rounded-circle shadow-2-strong" width="130px" height="130px" alt="avatar" />    
              
              </div>
         
          <div className="text-center" style={{marginBottom: "25%"}}>
              
              <h6>Amjad Khan</h6>   
              
              </div>
          
            <div
              className="nav flex-column nav-pills text-center"
              id="v-pills-tab"
              role="tablist"
              aria-orientation="vertical" >
              <a
                className="nav-link active"
                id="v-pills-home-tab"
                data-mdb-toggle="pill"
                href="#v-pills-home"
                role="tab"
                aria-controls="v-pills-home"
                aria-selected="true"
                >Personal Info</a
              >
              <a
                className="nav-link"
                id="v-pills-profile-tab"
                data-mdb-toggle="pill"
                href="#v-pills-profile"
                role="tab"
                aria-controls="v-pills-profile"
                aria-selected="false"
                >New Cases</a>
              <a
                className="nav-link"
                id="v-pills-messages-tab"
                data-mdb-toggle="pill"
                href="#v-pills-messages"
                role="tab"
                aria-controls="v-pills-messages"
                aria-selected="false"
                >Accepted Cases</a
              >
              <a
                className="nav-link"
                id="v-pills-messages-tab"
                data-mdb-toggle="pill"
                href="#v-pills-messages"
                role="tab"
                aria-controls="v-pills-messages"
                aria-selected="false"
                >Spam Cases</a
              >
              <a
                className="nav-link"
                id="v-pills-setting-tab"
                data-mdb-toggle="pill"
                href="#v-pills-setting"
                role="tab"
                aria-controls="v-pills-setting"
                aria-selected="false"
                >Settings</a
              >
              
            </div>
            <div className="text-center mb-3" style={{marginTop: "50%"}}>
              <button className="btn btn-primary" style={{width: "120px"}}>
                  Logout
              </button>
          </div>
           
          </div>
        
          <div className="col-9">
  
  
       
            <div className="tab-content" id="v-pills-tabContent">
              <div
                className="tab-pane fade show active"
                id="v-pills-home"
                role="tabpanel"
                aria-labelledby="v-pills-home-tab"
              >
              <div className="mt-4 text-center">
                <h4>Spam Case</h4>
              </div>
                
              <form className="my-5 mx-4">
               
                
                  <div className="mb-4 " style={{width: "48.5%",position:"absolute",left:"35%"}}>
                  <table class="table align-middle mb-0 bg-white"
                   >
  <thead class="bg-light">
  <tr>
      <th style={{color:"black"}}>Case Id</th>
      <th style={{color:"black"}}>CaseTitle</th>
      <th style={{color:"black"}}>Approval</th>
      <th style={{color:"black"}}>Denied</th>
      <th style={{color:"black"}}>View</th>
    
    </tr>
    </thead>
    <tbody>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Denny
        </button>
        
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
        
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
       
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
   
    <tr>
      <td>
        <div className="d-flex align-items-center">
         
          <div className="ms-3">
            <p className="fw-bold mb-1">1</p>
           
          </div>
        </div>
      </td>
      <td>
        <p className="fw-normal mb-1">Education Case</p>
        
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
      
      </td>
      <td>
      <button type="button" className="btn btn-link btn-sm btn-rounded">
          Accept
        </button>
      
      </td>
   
      <td>
        <button type="button" className="btn btn-link btn-sm btn-rounded">
          View
        </button>
       
      </td>
    </tr>
   
    
   
    
     
       
  </tbody>
</table>

    
                    
                  </div>
                
              </form>
              </div>
              </div>
              </div>
              </div>
              </div>
              </section>
              <footer className="text-center text-lg-start  text-muted" style={{backgroundColor: "#4d4d4f"}}>
   
   <section className="d-flex justify-content-center justify-content-lg-between p-4 border-bottom"
   style={{marginTop: "10%", color: "white"}}>

   <div className="me-5 d-none d-lg-block">
       <span>Get connected with us on social networks:</span>
   </div>

   <div>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-facebook-f"></i>
       </a>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-twitter"></i>
       </a>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-google"></i>
       </a>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-instagram"></i>
       </a>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-linkedin"></i>
       </a>
       <a href="/" className="me-4 text-reset">
       <i className="fab fa-github"></i>
       </a>
   </div>

   </section>

   <section style={{ height:"240px" }}>
   <div className="container text-center text-md-start mt-5" style={{backgroundColor: "#4d4d4f", color: "white"}}>
   
       <div className="row mt-3">
       
       <div className="col-md-3 col-lg-4 col-xl-3 mx-auto mb-4">
       
           <h6 className="text-uppercase fw-bold mb-4">
           <i className="fas fa-gem me-3"></i>Social Welfare
           </h6>
           <p>
           Here you can use rows and columns to organize your footer content. Lorem ipsum
           dolor sit amet, consectetur adipisicing elit.
           </p>
       </div>
   
       <div className="col-md-2 col-lg-2 col-xl-2 mx-auto mb-4">
   
           <h6 className="text-uppercase fw-bold mb-4">
           Navigation
           </h6>
           <p>
           <a href="#!" className="text-reset">Angular</a>
           </p>
           <p>
           <a href="#!" className="text-reset">React</a>
           </p>
           <p>
           <a href="#!" className="text-reset">Vue</a>
           </p>
           <p>
           <a href="#!" className="text-reset">Laravel</a>
           </p>
       </div>
       

       
       <div className="col-md-3 col-lg-2 col-xl-2 mx-auto mb-4">
           
           <h6 className="text-uppercase fw-bold mb-4">
           Explore
           </h6>
           <p>
           <a href="#!" className="text-reset">Pricing</a>
           </p>
           <p>
           <a href="#!" className="text-reset">Settings</a>
           </p>
           <p>
           <a href="#!" className="text-reset">Orders</a>
           </p>
           <p>
           <a href="#!" className="text-reset">Help</a>
           </p>
       </div>
       

       
       <div className="col-md-4 col-lg-3 col-xl-3 mx-auto mb-md-0 mb-4">
   
           <h6 className="text-uppercase fw-bold mb-4">
           Contact
           </h6>
           <p><i className="fas fa-home me-3"></i> New York, NY 10012, US</p>
           <p>
           <i className="fas fa-envelope me-3"></i>
           info@example.com
           </p>
           <p><i className="fas fa-phone me-3"></i> + 01 234 567 88</p>
           <p><i className="fas fa-print me-3"></i> + 01 234 567 89</p>
       </div>
       
       </div>
   
   </div>
   </section>

</footer>
              </div>

    </>
  )
}

export default SpamCase