import React from 'react'

export default function HomeProgessSection() {
  return (
    <div className="container">
      <section style={{marginTop: "35%"}}>
        <div className="container-fluid   shadow-4-strong"
          style={{height: "200px", backgroundColor: "#0C56D0", position: "relative", borderRadius: "10px"}}>

          <div style= {{height: "200px", width: "330px", position: "absolute", marginLeft: "2%", textAlign: "left" }} >
            <img src="./img/4.png" alt='' style= {{ marginTop: "18%", float: "left" }} />
            <h3 style={{ color: "white",  marginTop: "23%", marginLeft: "30%" }}>35+ Cases</h3>
          </div>

          <div style= {{height: "200px", width: "330px", position: "absolute", marginLeft: "35%", textAlign: "left" }}>
            <img src="./img/2.png" alt='' style= {{ marginTop: "18%", float: "left" }}  />
            <h3 style={{ color: "white",  marginTop: "21%", marginLeft: "30%" }}>12M People Joined</h3>
          </div>

          <div style= {{height: "200px", width: "330px", position: "absolute", marginLeft: "68%", textAlign: "left" }}>
            <img src="./img/3.png"alt='' style= {{ marginTop: "18%", float: "left" }}  />
            <h3 style={{ color: "white",  marginTop: "20%", marginLeft: "30%" }}>12000 Peoples <br />get help from us</h3>


          </div>
        </div>
      </section>
    </div>
  )
}
