import React from 'react';
import ReactDOM from 'react-dom/client';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import App from "./App";
import Home from "./components/Home";
import Contactinformation from './components/Contactinformation';
import AboutPage from './components/HomeAbout';
import ContactPage from './components/ContactPage';
import Vlanding from "./components/Vlanding";
import Notification from './components/Notification';
import Projects from "./components/Projects";
import RecentCase from "./components/RecentCase";
import Question from './components/Question';
import Documentsv from './components/Documentsv';
import NewCases12 from './components/NewCases12';
import AllVolunteer from './components/ApprovedVolunteer';
import ApplyNowRules from "./components/ApplynowRules";
import AllEvent from "./components/AllEventPage";

import DashboardApplicant from "./components/DashboardApplicant";
import Middlepage from "./components/Middlepage";
import PayMethodOptions from './components/PayMethodOptions';
import SignInSignUp from './components/SignInSignUpPage';
import ProjectDescription from './components/ProjectDescription';
import EventDescription from './components/EventDescription';
import Blood from './components/Blood';
import Application from './components/Application';
import Hungry from './components/Hungry';
import AcceptedCase from './components/AcceptedCase';
import Dashboard from './components/Dashboard';
import NewCases from './components/NewCases';
import SpamCase from './components/SpamCase';
import Setting from './components/Setting';
import AllEventPage from './components/AllEventPage';
import AllProjectPage from './components/AllProjectPage';
import ApplynowInfo from './components/ApplynowInfo';
import CardDetail from './components/CardDetails';
import MobilePay_1 from './components/MobilePay_1';
import CashByHand from './components/CashByHand';
import ResetPasswordPage from './components/ResetPasswordPage';
import Applynowaddress from './components/Applynowaddress';
import Applynowcontact from './components/Applynowcontact';
import ApplyNowDocs from './components/ApplyNowDocs';
import ApplynowBank from './components/ApplynowBank';
import Dashboard1 from './components/Dashboard1';
import Newcases1 from './components/Newcases1';

import Spamcases1 from './components/Spamcases1';
import Approvedcases1 from './components/Approvedcases1';
import Newvolunteerlist from './components/Newvolunteerlist';
import SpamVolunteers from './components/SpamVolunteers';
import AddnewProject1 from './components/AddnewProject1';
import AddnewEvent2 from './components/AddnewEvent2'; 
import ReasonSpam from './components/ReasonSpam';
import Document from './components/Document';
import GuideLines from './components/GuideLines';
import NewVolunteer from './components/NewVolunteer';
import reportWebVitals from './reportWebVitals';
import ApprovedVolunteer from './components/ApprovedVolunteer';
import ReportPage from './components/ReportPage';
import HomeAbout from './components/HomeAbout';



const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <BrowserRouter>
    <Routes>
      <Route  exact path='/' element={<App />} />
      <Route path="home" element={<Home />} />
      <Route path="/rep" element={<ReportPage />} />
      <Route path="/volunteer" element={<Vlanding />} />
      <Route path="/recentcase" element={<RecentCase />} />
      <Route path="/notify" element={<Notification />} />
      <Route path="/approv" element={<ApprovedVolunteer />} />
      <Route path="/allevents" element={<AllEvent />} />
      <Route path="/dashboardapplicant" element={<DashboardApplicant />} />
      <Route path="/paymethodoption" element={<PayMethodOptions />} />
      <Route path="/applynowrule" element={<ApplyNowRules />} />
      <Route path="/contact" element={<ContactPage />} />
      <Route path="/middlepage" element={<Middlepage />} />
      <Route path="/ques" element={<Question />} />
      <Route path="/signin" element={<SignInSignUp />} />
      <Route path="/projectdescrip" element={<ProjectDescription />} />
      <Route path="/eventdescrip" element={<EventDescription />} />
      <Route path="/hungry" element={<Hungry />} />
      <Route path="/blood" element={<Blood />} />
      <Route path="/application" element={<Application/>} />
      <Route path="/dashboard" element={<Dashboard/>} />
      <Route path="/doc" element={<Documentsv/>} />
      <Route path="/newcase" element={<NewCases/>} />
      <Route path="/setting" element={<Setting/>} />
      <Route path="/spamcase" element={<SpamCase/>} />
      <Route path="/allproj" element={<AllEventPage/>} />
      <Route path="/allevent" element={<AllProjectPage/>} />
      <Route path="/appinfo" element={<ApplynowInfo/>} />
      <Route path="/cashbyhand" element={<CashByHand/>} />
      <Route path="/mobpay" element={<MobilePay_1/>} />
      <Route path="/carddet" element={<CardDetail/>} />
      <Route path="/rest" element={<ResetPasswordPage/>} />
      <Route path="/apcont" element={<Applynowcontact/>} />
      <Route path="/apadr" element={<Applynowaddress/>} />
      <Route path="/apdoc" element={<ApplyNowDocs/>} />
      <Route path="/apbank" element={<ApplynowBank/>} />
      <Route path="/dash1" element={<Dashboard1/>} />
      <Route path="/newcase1" element={<Newcases1/>} />
      <Route path="/spamcases1" element={<Spamcases1/>} />
      <Route path="/projects" element={<Projects/>} />
      <Route path="/app1" element={<Approvedcases1/>} />
      <Route path="/app2" element={<Newvolunteerlist/>} />
      <Route path="/app4" element={<SpamVolunteers/>} />
      <Route path="/app5" element={<AddnewProject1/>} />
      <Route path="/app6" element={<AddnewEvent2/>} />
      <Route path="/app7" element={<ReasonSpam/>} />
      <Route path="/app8" element={<Document/>} />
      <Route path="/app9" element={<AcceptedCase/>} />
      <Route path="/app10" element={<GuideLines/>} />
      <Route path="/app15" element={<HomeAbout/>} />
      <Route path="/app11" element={<NewCases12/>} />
      <Route path="/app12" element={<NewVolunteer/>} />
   
      <Route path="/coninfo" element={<Contactinformation />} />
    </Routes>
    </BrowserRouter>
</React.StrictMode>
);

reportWebVitals();
